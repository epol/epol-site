---
layout: page
title: Projects
permalink: /projects/
license: CC_BY-SA
---

Here there is a list of my projects (of course it is very short).

## Computer related

### Programming

#### ACTS

_ACTS_ (Automatic cannon targeting system) is a toy project that I
wrote in 2015 with [Andrea Stacchiotti](https://github.com/12345ieee)
for a C++ class. Its aim is to provide a simple targeting computer
for an hypothetical cannon that can correct himself using data from
past throws.

The source code is hosted on <span class="icon icon--github">{% include icon-github.svg %}</span> 
[epol/acts](https://github.com/epol/acts).

#### Dromd

Dromd (/ˈdrɒmədɛri/) is a programming language that I wrote with
[Alessandro Achille](https://uz.sns.it/~alex/) for a University
exam. It has very basic functions and a very difficult syntax
(actually no syntax at all: you have to write the semantic in
OCaml), so it's useless.

But I can say that I have my own programming language.

The source code is hosted on
<span class="icon icon--github">{% include icon-github.svg %}</span>
[epol/dromd](https://github.com/epol/dromd)

### Websites

#### This site

This website is statically generated using
[jekyll](http://jekyllrb.com). So nothing fancy.

#### mk8

_mk8_ is the web platform of the 2016
[Collegio Timpano](http://timpano.sns.it) Mario Kart 8 competition.

It is just a site where the players can find their matches and can
upload the results. The website automatically computes the points and
the ranks.

I wrote this site with
[Dario Balboni](http://poisson.phc.unipi.it/~balboni/) using
[django](https://www.djangoproject.com): a high level python web
framework _for perfectionists with deadlines_ (we wrote all the core
functions of the site and the graphic interface in just one night).

The site is still online at
[mk8.starfleet.sns.it](https://mk8.starfleet.sns.it/) and the code is
available on <span class="icon icon--github">{% include
icon-github.svg %}</span> [starfleetsns/mk8](https://github.com/starfleetsns/mk8).

## Mathematic


#### Bachelor thesis

One day there will be some description here. For now just the link to
<span class="icon icon--github">{% include icon-github.svg %}</span>
[epol/tesi3](https://github.com/epol/tesi3) and to its revised version
<span class="icon icon--github">{% include icon-github.svg %}</span>
[epol/colloquio4](https://github.com/epol/colloquio4).

#### TMO seminar

For the [TMO](http://pages.di.unipi.it/bigi/dida/tmo.html) exam
(theory and methods of the optimization) I did a seminar on how to
predict network patterns using the game theory, basically it's about
some basic results of:

* Wardrop equilibria;
* Price of Anarchy;
* Braess's paradox;
* partially optimal routing;
* competition in congested markets.

The latex sources of all the material (in italian) are available at
<span class="icon icon--github">{% include icon-github.svg
%}</span>[epol/seminariotmo](https://github.com/epol/seminariotmo).

## Electronic

Coming someday.
