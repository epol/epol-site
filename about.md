---
layout: page
title: About
permalink: /about/
license: CC_BY-SA
---

Some boring informations.

## About me

I'm a graduate student in mathematics at University of Pisa, I'm
interested in algorithms, numerical analysis and sometimes also in
other things like probability and mathematical finance.

I also work (little) in IT where I play with networks (both wired and
wireless), sometimes I also play with VMs, servers and things like
that. I'm interested in High Availability solutions, software defined
networking and everything that sounds cool and innovative. My
favourite operative system is, of course, Linux. Debian for servers
and Fedora for my PC. (OK, I confess, when I feel brave I use gentoo
on my PC).

I'm a mediocre programmer, for this reason I've never wrote any
interesting program but I wrote many scripts, little programs and some
website. My programming language of choice is python, sometimes I use
bash and, when I'm very desperate, C/C++.

Since I don't want to move too much away from computers, my hobby is
doing electronics. I started programming Arduino, then I tried ICs and
now I'm moving to analog circuits. I'm not cool enough to print my own
PCB and most of my circuits never leave the breadboard phase.



## About this site

This site was born as my fourth (in chronological order) blog,
basically it is just the conversion in a static form of the
[old wordpress blog](https://uz.sns.it/~enrico/wordpress/), then I
added the [projects page]({{ "/projects/" | relative_url }}) and so I
changed the title from _blog_ to _website_.

The main purpose of the blog is to share things that I think that may
be useful to others and that I didn't find easily online, so it can be
just the collage of other resources or a original work. But the
original contents will be negligible. The rest of the site is made up
of other random things.

This site is powered by [jekyll](http://jekyllrb.com), a static
website generator written in ruby (but I'm considering the idea to
move to [hyde](https://hyde.github.io) just because I like python),
with [git](https://git-scm.com) as version control system (with a
repository hosted on <span class="icon icon--github">
{% include icon-gitlab.svg %}</span>
[epol/epol-site](https://gitlab.com/epol/epol-site) ) and the
[minima](https://github.com/jekyll/minima) theme (with some small
edit).

The main mirror of this site is hosted in the [UZ
infrastructure](https://uz.sns.it) at Scuola Normale Superiore and it's
available at this [address](https://uz.sns.it/~enrico/site/). There is
a mirror hosted on [surge.sh](https://surge.sh) deployed with [GitLab
CI](https://about.gitlab.com/gitlab-ci/) available at
[epol.surge.sh](https://epol.surge.sh).

### <a name="licenses"></a>Licenses

My original contents in the site are licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International
License](http://creativecommons.org/licenses/by-sa/4.0/).

The minima theme is Copyright &copy; 2016 Parker Moore and licensed
under the [MIT License](https://opensource.org/licenses/MIT), my
changes are under the same license.

The jekyll generator is Copyright &copy; 2008-2016 Tom Preston-Werner
and available under the terms of the
[MIT License](https://opensource.org/licenses/MIT).

Any different license will be declared with the content it covers.

## Contact me and external links

Me on the web:

* Github: {% include icon-github.html username=site.github_username %}
* Twitter: {% include icon-twitter.html username=site.twitter_username %}
* Gitlab.com: {% include icon-gitlab.html username=site.gitlab_username %}
* Reddit: [u/saggiopol](https://www.reddit.com/user/saggiopol)
* Linkedin: [in/polesel](https://www.linkedin.com/in/polesel)

A email address: [{{site.email}}](mailto:{{site.email}})
