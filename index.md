---
layout: default
---

Welcome to my website, probably you are here because of some glitch of
your browser, so [here](https://duckduckgo.com) there is something
more interesting.

If you are really interested in this website then here there is a list
of resources:

* [About page]({{ "/about/" | relative_url }})
* [My blog posts]({{ "/posts/" | relative_url }})
* [My projects]({{ "/projects/" | relative_url }})
 
