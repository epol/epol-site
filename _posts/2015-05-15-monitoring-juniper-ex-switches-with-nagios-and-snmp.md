---
layout: post
title:  "Monitoring Juniper EX switches with nagios and SNMP"
date:   2015-05-15 22:22:25 +0200
categories: networking monitoring
tags:
  - icinga
  - juniper
  - nagios
  - SNMP
license: CC_BY-SA
--- 


I want to monitor some juniper EX 2200 and EX4200 switches, in
particular i want to know: if all members of the virtual chassis are
online, if their temperatures are OK and if some particular interfaces
are up (and if the speed is correct).

So I decided to use icinga (a nagios fork) to run the monitoring and
to use SNMP to get the data.

The basic configuration of SNMP is pretty simple:

	[edit] set snmp community readonly_community authorization read-only

When all the switches are configured for SNMP I looked in the juniper
MIB and i found:

 * _jnxVirtualChassisMemberRole_: a table with the role of all members
   (if the member is not present then its entry won't be present in
   this table)
 * _jnxOperatingTemp_: many tables with many temperature, i was
   interested in the routing engine temperatures so i used the
   _jnxOperatingTemp.7_ table
 * _ifName_: a table to look for a specific interface and
   _ifHighSpeed_ to get its speed (note that _ifSpeed_ may be not
   useful when the interface is faster than 2^32bit/s ~ 4.5Gbit/s)

So I wrote _getvcn.c_ to get members status, _gettempn.c_ to get
temperatures and _lookif.c_ to get interface informations. All this
code is available under the GPLv2 license in
[github](https://github.com/epol/snmp). I haven't written an help
(yet), but the options can be easily deduced from the source code.

The configuration of icinga (nagios) was very simple, i defined my
custom commands to call my programs and then i defined services that
calls them.
