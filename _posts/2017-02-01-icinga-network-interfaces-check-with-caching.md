---
layout: post
title:  "Icinga network interfaces check with caching"
date:   2017-02-01 18:22:25 +0100
categories: networking monitoring
tags:
  - icinga
  - SNMP
  - icinga REST api
license: CC_BY-SA
--- 

In our configuration of icinga2 we refer to interfaces with their name
(since it is easier to configure) while the SNMP query uses the
interface index.

In my old check script I used to walk the _ifName_ table looking for
the association index-interface, knowing the index I was able to get
the informations I wanted (status and statistics).

This operation could be expensive since there may be many interfaces
in the table (think, for example, at the case of a switch), so knowing
the index would speed things up.

A way to do it would be to use the index to configure the service
instead of the name, but this would worsen the configuration
process. So I add a caching mechanism to icinga to save the SNMP
interface index, in this way the lookup would be performed only the
first time.

To do this I modified my interface check script with the following
logic:

 1. get from command line the interface name and the expected SNMP
    index (if available);
 2. check if the interface at that index has the expected name;
 3. if the name doesn't match (or the index wasn't available) perform
    a interface lookup and update the SNMP index saved in icinga;
 4. do the usual checks and return.
 
I wrote the script in python (and it's available on
[github](https://github.com/epol/snmp/blob/master/lookif.py)) using
the _easysnmp_ library to do the SNMP operations, and the requests
library to update the SNMP index values in icinga using
the icinga REST API with a dedicated user configured with:
{% highlight conf %} 
object ApiUser "linkcheck" {
	password = "mysecretpassword"
	permissions =[
		{
			permission = "objects/modify/Service"
			filter = {{ "linkcheck in service.templates" }}
		},
		{
			permission = "objects/query/Service"
			filter = {{ "linkcheck in service.templates" }}
		}
	]
}
{% endhighlight %}

And the command is configured with something like this:
{% highlight conf %}
object CheckCommand "lookif" {
       import "plugin-check-command"
       command = [ CustomPluginDir + "/lookif.py" ]

       arguments = {
         "-H" = "$address$"
		 "-C" = "$snmp_community$"
		 "-i" = "$interface$"
		 "-w" = "$interface_speed$"
		 "-n" = "$interface_index$"
		 "-iH" = "icingahostname"
		 "-iu" = "linkcheck"
		 "-ip" = "mysecretpassword"
       }
}
{% endhighlight %}

Then link checks are configured using the `linkcheck` template:
{% highlight conf %}
template Service "linkcheck" {
  import "generic-service"
  check_command = "lookif"
  vars.interface_speed = "1000"
}
{% endhighlight %}
