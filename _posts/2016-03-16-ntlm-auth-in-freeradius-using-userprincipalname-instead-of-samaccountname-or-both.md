---
layout: post
title:  "ntlm auth in FreeRADIUS using userPrincipalName instead of sAMAccountName (or both)"
date:   2016-03-16 17:11:18 +0100
categories: networking
tags: 
  - active directory
  - freeradius
  - ldap
  - ntlm_auth
  - samaccountname
  - userprincipalname
license: CC_BY-SA
---

## Theory of operations

Let’s assume we have a FreeRADIUS server that uses Active Directory as
a authentication backend via the `ntlm_auth` tool. The expected
username for `ntlm_auth` is the one called __sAMAccountName__ but we
may want to permit (or force) our users to use the
__userPrincipalName__ (for more infos about AD user naming see
[this](https://msdn.microsoft.com/en-us/library/windows/desktop/ms677605%28v=vs.85%29.aspx)
page of the MSDN), so we have to implement a __translation from one
name to the other__.

In this post I assume that we have one Active Directory server with one domain: _example.com_ in the DNS and _EXAMPLE_ in the NetBIOS (note that they can be completely different). The _userPrincipalNames_ will something like "username@example.com" and the _sAMAccountname_ something like "user" (that becomes "EXAMPLE\user" in the NetBIOS convention).

We want to authenticate all the possible combinations like:

* username@example.com
* username
* user@example.com
* user

This configuration can be used both with and without EAP tunnels, with both PAP and MSCHAPv2 authentication.

The suffix isn’t a problem since we can use the `Stripped-User-Name`
field, the problem is **translating the _userPrincipalName_ to the
_sAMAccountname_**, one way to make this translation is to __look for
the username in the Active Directory LDAP database and extract the
desired field__, to do this we can write a wrapper to the ntlm\_auth
command (for example in Python) or we can instruct the freeradius
server to make the query and pass the proper parameter to
ntlm\_auth. I choose the second method, because configuring LDAP
enables other nice functionalists (for example group authorizing). I
took the basic ideas from [this
message](http://lists.freeradius.org/pipermail/freeradius-users/2016-January/081653.html).

So we configure the server to call the _ldap_ module during the
authorization phase (that precedes the authentication phase) in order
to search the user using his _userPrincipalName_ (or both the
_userPrincipalName_ and the _sAMAccountName_) and we map the
_sAMAccountName_ to a __custom RADIUS attribute__ that we can use in
the `ntlm_auth` command.

## Configuration

**Update** (30 Jun 2017): I've reviewed the post with instruction for
  FreeRADIUS 3. The configurations for FR 2 are listed at the end of
  the post, but I recommend to update to v3.

### Configuration for FreeRADIUS 3.0.14

I did this configuration in a Ubuntu 16.04 server with a self compiled
FreeRADIUS 3.0.14 from git. Compiling from sources is
[easy](http://wiki.freeradius.org/building/Debian%20and%20Ubuntu) and
I recommend to use the last stable version. Since I use the debian
packets, for me `${confdir}` is the `/etc/freeradius/` directory and
the radius server can be started with the `freeradius` command.

First of all we need to configure the freeradius server to use ntlm\_auth to query the AD, it is well documented in the freeradius wiki, for example [here](http://wiki.freeradius.org/guide/FreeRADIUS-Active-Directory-Integration-HOWTO) and [here](http://wiki.freeradius.org/guide/NTLM-Auth-with-PAP-HOWTO). I had some problem with the winbind privileges that i solved with [this](http://xenomorph.net/linux/samba/issues/exec-program-output-reading-winbind-reply-failed/) guide. With this configuration you should be able to use radtest to authenticate using the _sAMAccountName_.

We need some variable to save the _sAMAccountName_, so we define a
__custom attribute__ in the `${configdir}/dictionary` file adding a
line with
{% highlight dictionary %}
ATTRIBUTE AD-Samaccountname 3003 string
{% endhighlight %}
where `3003` is just some unused oid.

Then we can configure the LDAP module editing the
`${confdir}/mods-available/ldap`, we need a valid account on the AD to
bind to the LDAP database (because of the specifications of Active
Directory).

The relevant parts of the configurations are:
{% highlight unlang %}
ldap{
....
    server = "ad.example.com"
    identity = "CN=radius,OU=someou,DC=example,DC=com
    password = "somepassword"
    base_dn = "DC=example,DC=com"
....
    update {
....
        control:AD-Samaccountname := 'sAMAccountName'
....
    }
    user {
    ....
        filter = "(|(userPrincipalName={{"%{%{"}}Stripped-User-Name}:-%{User-Name}}@example.com)(sAMAccountName={{"%{%{"}}Stripped-User-Name}:-%{User-Name}}))"
    }
....
}
{% endhighlight %}

It’s important to notice the filter field, we are asking the LDAP
__"give me the users with principalUserName=user-name@example.com or
sAMAccountName=user-name"__ where "user-name" is the request stripped
username. If we want to force our users to use only the
_userPrincipalname_ the filter will be:
{% highlight unlang %}
filter = "(userPrincipalName={{"%{%{"}}Stripped-User-Name}:-%{User-Name}}@example.com)"
{% endhighlight %}

We can now enable the ldap module linking it to the
`${configdir}/mods-enabled` directory and calling it in the
authorization section of the virtual server we are using (probably
`${configdir}/sites-enabled/default` and
`${configdir}/sites-enabled/inner-tunnel`). We call it in the
_authorize_ section because we need to know the sAMAccount name
__before__ authentication.

Finally we have to edit all the places where we call the ntlm\_auth
attribute (in my configuration
`${configdir}/mods-available/ntlm\_auth_` e
`${configdir}/mods-available/mschap`) replacing the
`--username=%{something}` parameter with
`--username=%{control:AD-Samaccountname}`

It also may be necessary to hardcode `--domain=EXAMPLE` (the NetBIOS domain name) in the command. Or you can write (in the freeradius unlang) your own conversion from the user realm (possibly _example.com_) in the NetBIOS domain (_EXAMPLE.COM_).

Restart the freeradius server and hopefully it should work. To debug use the radtest tool and execute freeradius in foreground with the `-X` option, it will print a lot of useful information about the authentication process. I must say that i really enjoy executing `freeradius -X` during debug.

I described a very simple setting, I think it can be extended (for example to authenticate more that one domain), but I leave it as an exercise for the reader (I think the tricky thing is to convert the DNS name example.com in the NetBIOS name EXAMPLE, tricky but not impossible).

### Configuration for FreeRADIUS 2.2.8

I did this configuration in a Ubuntu 14.04 server with the packetized freeradius, for me `${confdir}` is the `/etc/freeradius/` directory.

We start configuring the LDAP module in freeradius. We edit the `${confdir}/modules/ldap`, we need a valid account on the AD to do this:
{% highlight unlang %}
ldap{
....
    server = "ad.example.com"
    identity = "CN=radius,OU=someou,DC=example,DC=com
    password = "somepassword"
    filter = "(|(userPrincipalName={{ "%{%{"}}Stripped-User-Name}:-%{User-Name}}@example.com)(sAMAccountName={{"%{%{"}}Stripped-User-Name}:-%{User-Name}}))"
....
}
{% endhighlight %}

As for the FR3 configuration, if you want to permit only the use of the __userPrincipalName__ you can use the following filter:
{% highlight unlang %}
filter = "(userPrincipalName={{"%{%{"}}Stripped-User-Name}:-%{User-Name}}@example.com)"
{% endhighlight %}

Before closing the ldap module file check that there is (uncommented) the following line:
{% highlight unlang %}
dictionary_mapping = ${confdir}/ldap.attrmap
{% endhighlight %}

Now we are making the "right" query to the ldap, but we aren’t saving the _sAMAccountname_ anywhere, first we need to define a __custom attribute__ in the `${configdir}/dictionary` file adding a line with

	ATTRIBUTE AD-Samaccountname 3003 string

Then we configure the __LDAP mapping__ adding to the `${configdir}/ldap.attrmap` the line

	replyItem AD-Samaccountname sAMAccountName

We can now call the LDAP module in the _authorize_ sections of our server.

Finally we have to edit all the places where we call the ntlm\_auth attribute (in my configuration _${configdir}/modules/ntlm\_auth_ e _${configdir}/modules/mschap_) replacing the `–username=%{something}` parameter with	`--username=%{reply:AD-Samaccountname}`

As usual, you can debug the server with the `freeradius -X` command.

