---
layout: post
title:  "Passwordless SSH login with kerberos and AFS"
date:   2014-03-08 22:31:12 +0100
categories:
  - kerberos
tags:
  - kerberos
  - afs
  - passwordless
  - ssh
  - keytab
  - kinit
license: CC_BY-SA
--- 

Suppose that you have a valid account in a kerberos-afs system (like
[UZ](http://uz.sns.it/) with ssh access and you want to connect
without typing the password every time. The standard method is to use
a ssh key pair for the authentication. But this simply doesn't work
with AFS.

The solution is to use kerberos to authenticate your computer, then
(with the proper options) you can login on the ssh server without any
password (but simply using your kerberos ticket).

So we have to do this:

 1. get a kerberos ticket
 2. use the ticket to authenticate against the ssh server

To make the process automatic we will also have to:

 * get the ticket without using the password but using a keytab file
 * automatically renew the ticket with `krenew`

##Install kerberos and use it to get a ticket 

Install the krb5 package, for example in Debian (and
Debian derivatives) the package is called `krb5-user`, in
Fedora is called `krb5-workstation`.

Now you can use kerberos to authenticate with
{% highlight bash %}
kinit your_user_name@EXAMPLE.COM
{% endhighlight %}

And you'll have a valid ticket.

##Connect to the ssh server

With the kerberos ticket the ssh server will let you in, but it won't
be able to get an afs token (because, by a security default, you don't
delegate your credetials to the server) and so you won't be able to
use afs (where usually there is your home directory).

To avoid this problem you have to set the following options:
{% highlight conf %}
GSSAPIAuthentication yes
GSSAPIDelegateCredentials yes
{% endhighlight %}

For example by passing them as a ssh parameter:
{% highlight bash %}
ssh -o GSSAPIAuthentication=yes -o GSSAPIDelegateCredentials=yes your_user_name@ssh.example.com
{% endhighlight %}

or inserting them in your ssh config file (`~/.ssh/config`). There are
two ways to do this: the first (and less secure) is to use that
parameters for all your outgoing ssh connections:
{% highlight conf %}
Host *
GSSAPIAuthentication yes
GSSAPIDelegateCredentials yes
{% endhighlight %}

The second is configure every host in this way:
{% highlight conf %}
Host shortcut
User your_user_name
Port 22
Hostname ssh1.example.com
GSSAPIAuthentication yes
GSSAPIDelegateCredentials yes
{% endhighlight %}

##Get the ticket with a keytab
First you have to create a keytab for you user
{% highlight bash %}
$ ktutil
ktutil: add_entry -password -p your_user_name@EXAMPLE.COM -k 1 -e aes256-cts-hmac-sha1-96
ktutil: write_kt ~/keytab
{% endhighlight %}

Now you have a keytab in your home directory (or where you save it),
make sure to protect it with the proper permissions.

You can use the keytab to get a kerberos ticket with
{% highlight bash %}
kinit -k -t ~/keytab your_user_name@EXAMPLE.COM
{% endhighlight %}

##Use krenew to renew the ticket

Install the proper package (`kstart` in Debian and Fedora) to get the
`krenew` command. After getting a ticket you can start the _krenew_
daemon with:
{% highlight bash %}
krenew -K 5 -b
{% endhighlight %}

##A kerberos initialization script

If you want to automatize the get _kinit_ and _krenew_ operation you
will need a script like this:
{% highlight bash linenos %}
#!/bin/bash
kinit -k -t ~/keytab your_user_name@EXAMPLE.COM
krenew -K 5 -b
{% endhighlight %}
