---
layout: post
title:  "Discovery and monitor meru AP with icinga2"
date:   2017-02-24 12:22:25 +0100
categories: networking monitoring
tags:
  - icinga
  - meru
  - wifi
  - SNMP
  - icinga REST api
license: CC_BY-SA
--- 

I want to monitor all the wireless access points in our Meru
installation (1 controller, ~250 AP) using one of our monitor systems:
icinga2 (a evolution of nagios).

In this post I describe the two main component of my setup: the check
scripts and the discovery script; both of them are available on
[github](https://github.com/epol/snmp/tree/master/meru). A third
(optional but useful) task is to add dependencies to the APs object so
that they depend on the switch they are connected to.

## Monitor scripts

Looking in the meru SNMP MIBs I found a table where I can find a lot
of informations about the APs, in fact there are both static
information (like the name, position, serial number,...) and dynamic
one (like operational status, IP, uptime), so I can easily write a
script (icinga still runs external scripts like nagios) that, given
the table index, checks the Operational and Availability status of the
AP in that row and returns the result in the icinga way.

Using python with the library `easysnmp` I write the two scripts
`meru_AvailabilityStatus.py` and `meru_OperationalState.py`: they take
as command line arguments the address of the meru controller, its SNMP
community and the AP index, they return a proper return code and a
short message. I also configure two custom commands in icinga that
runs this scripts.
{% highlight conf %}
object CheckCommand "meru_OperationalState" {
       import "plugin-check-command"
       command = [ CustomPluginDir + "/meru_OperationalState.py" ]

       arguments = {
       		 "-m" = "$controller$"
                 "-C" = "$snmp_community$"
                 "-i" = "$index$"
       }
}

object CheckCommand "meru_AvailabilityStatus" {
       import "plugin-check-command"
       command = [ CustomPluginDir + "/meru_AvailabilityStatus.py" ]

       arguments = {
       		 "-m" = "$controller$"
                 "-C" = "$snmp_community$"
                 "-i" = "$index$"
       }
}
{% endhighlight %}

Now I have to add the APs objects to the monitor system, I want every
AP to be a host with a custom check command (I don't get the UP/DOWN
state with a ping but with a SNMP check) and two services: the
availability and the operational status.

Instead of using the usual `generic-host` template I create a new
template `meru-AP` and the apply rules for the two services adding the
following lines to the configuration:
{% highlight conf %}
template Host "meru-AP" {
  max_check_attempts = 2
  check_interval = 5m
  retry_interval = 1m
  enable_flapping = true
  check_command = "meru_OperationalState"
  vars.os = "MeruAP"
  vars.controller = "MeruControllerHostname"
  vars.snmp_community = "MeruControllerCommunity"
}

template Service "meru-service" {
  max_check_attempts = 5
  check_interval = 5m
  retry_interval = 1m
  enable_flapping = true
}

apply Service "availabilityStatus" {
  import "meru-service"
  check_command = "meru_AvailabilityStatus"
  assign where "meru-AP" in host.templates
}

apply Service "operationalState" {
  import "meru-service"
  check_command = "meru_OperationalState"
  assign where "meru-AP" in host.templates
}
{% endhighlight %}

Now I could add a AP host to the monitor simply including the
_meru-AP_ template and specifying the index of the AP in the SNMP
table writing something like this:
{% highlight conf %}
object Host "AP test" {
	import "meru-AP"
	vars.index = 14
}
{% endhighlight %}

## Discovery of the APs

With the monitor scripts and configuration I can add objects, but I
would have to configure them manually (they are ~250) and keep the
SNMP table indexes updated. This is a good thing to automate because
the AP are too many and to reduce human errors.

I can get the list of the APs (and their proprieties) from the meru
controller using SNMP, I can generate the config files and load them
or add the configuration using icinga REST API, I prefer the second
way since it doesn't require to restart icinga and to touch config
files.

The first thing to do is to set up a API user in icinga with the
permission to get and modify the configuration, with this user I can
do this operations:

 * PUT: add a new AP to icinga
 * GET: get the configuration of AP
 * DELETE: remove an AP from icinga (useful to get rid of old APs)

So now I write a python script (`merudiscovery.py`) that gets the list
of all APs from the meru controller and from icinga, it compare them
and update the icinga object using the APIs. Since it's so easy to do
it I also get other propreties from the controller and add them as
vars to icinga (for example the hwtype, the building,...).

The script takes as command line arguments the meru hostname and
community, the icinga hostname, username and password, it does the
operations and at the end it prints the counter of add, remove and
update operations.

The script also mangages dependencies (as it is described in the next
sections), if it is not needed you can just comment it out.

## Dependencies

What happens if the controller goes down? All the AP host objects will
go DOWN and so many misleading alerts would be fired while only one
(the controller is down) is really interesting. This can be solved
easily adding a dependency to every AP to the controller with the
following configuration (_MeruController_ would be the name of the
controller object in icinga):
{% highlight conf %}
apply Dependency "AP-controller" to Host {
	parent_host = "MeruController"
	
	assign where "meru-AP" in host.templates
}
{% endhighlight %}

Another thing that may happen is that the switch connected to the AP
may go down and so extra alerts would be fired again, this time the
solution is not so easy like before.

For every AP I need to know which switch it is connected too, since I
don't have any automatic way to do this I just add this information
in the floor field in the AP configuration on the meru controller,
this field will contain the name (as icinga knows it) of the switch
and the discovery script will use this information to add a dependency
(with a extra check to see if a switch with that name exists).

In our setup we also use Juniper virtual chassis technology, so
knowing that the switch (in this case the virtual chassis) is up
wouldn't be enough, I have to add a dependency to the member service
(the actual physical switch). The discovery script also takes care of
this using the port name information that is saved in the contact
variable of the AP.

I could have configured dependency with icinga apply rules, I decided
against it because I wanted to add extra checks (for example that the
switch actually exists) and regular expression matching in the code,
maybe it could be done in the icinga configuration language, but I
felt more confortable with python.

## Possible improvements

My setup works and does what I need, but there are some small thing I
can improve:

 * __support for multiple controllers__: at the moment I have only one
   meru controller, so its hostname is written is some
   configurations. It would be easy to extend the scripts to support
   multiple controllers;
 * __icinga API certificate check__: now I'm ignoring the SSL
   certificate provided by the icinga API server, this is highly
   insecure and I'm mitigating it running the discovery script on the
   monitoring master;
 * __authentication of icinga API__: instead of using password
   authentication certificates would be more secure.
