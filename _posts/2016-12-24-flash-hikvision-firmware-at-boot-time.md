---
layout: post
title:  "Upgrade HikVision firmware at boot time"
date:   2016-12-24 23:30:18 +0100
categories: videosurveillance
tags: 
  - hikvision
  - firmware
  - debrick
  - tftp
  - boot
license: CC_BY-SA
---

It is possible to flash a HikVision NVR (and probably also the
cameras) during early boot (so that it is effective also when the
device is bricked) using a TFTP server with the right configuration

I managed to do it (with some trial and error). In this post I'll
tell you how I found it out, what I learned of the process and how to
do it in practice.

__I wrote this post just to share what I find out, it doesn't come
from official documentation and I'm not responsible for any damage you
may do to your devices__

## My little story

When using a device like a HikVision NVR it may be necessary to update
its firmware and in some (hopefully rare) cases it may give
problems. In my case it wasn't working properly (I was experiencing a
lot of reboots) and it was rejecting all firmware upgrades from the
interface. So I had to recover it like it was bricked.

From a previous network sniff I noticed (from ARP packets) that when
you turn on the NVR it tries to contact the address `192.0.0.128`, so
I imagined that there was some kind of recovery mechanism.

When I had the corrupted firmware problem the first thing I tried was
to put a TFTP server on the newtork with that address serving the
firmware file, but it didn't work because the first thing the NVR does
is to send a particular UDP packet (more on this later) and if it
receives no answer it just continues with the normal boot sequence.

With some internet searches I find out that HikVision has its own TFTP
flash tool (not so easy to find where to download) for Windows,
unfortunately it doesn't work well: the connection is opened, the file
is transfered to the NVR but for some reason it doesn't complete with
success and it just stucks retrying to get the firmware again and
again.

Sniffing (again) the network traffic permitted me to understand the
process and to recreate it on my linux box (just because I feel more
confortable there, it should be possible to do it also on Windows and
macOS).

## Theory of operations

When the NVR boots up it tries to contact `192.0.0.128` using the
`192.0.0.64` address, the first packet it sends is a __UDP packet__ to
the __port 9978__ with some string (I don't know its meaning yet and
if it constant or not) and the server replies with the __same
string__.

The packets will look something like this:
{% highlight tcpdump on %}
ARP, Request who-has 192.0.0.128 tell 192.0.0.64, length 46
ARP, Reply 192.0.0.128 is-at [servermac], length 28
IP 192.0.0.64.9979 > 192.0.0.128.9978: UDP, length 20
IP 192.0.0.128.9978 > 192.0.0.64.9979: UDP, length 20
{% endhighlight %}

At this point the NVR resends the ARP request (maybe it has resetted
its cache after booting some recovery subsystem) and the proceeds to
request via __TFTP__ the file `digicap.dav` (the new firmware).

Again sniffing the packets would look like this:
{% highlight tcpdump on %}
ARP, Request who-has 192.0.0.128 tell 192.0.0.64, length 46
ARP, Reply 192.0.0.128 is-at [servermac], length 28
IP 192.0.0.64.3570 > 192.0.0.128.69:  51 RRQ "digicap.dav" octet timeout 1 tsize 0 blksize 1468
IP 192.0.0.128.57989 > 192.0.0.64.3570: UDP, length 40
IP 192.0.0.64.3570 > 192.0.0.128.57989: UDP, length 4
IP 192.0.0.128.57989 > 192.0.0.64.3570: UDP, length 1472
IP 192.0.0.64.3570 > 192.0.0.128.57989: UDP, length 4
IP 192.0.0.128.57989 > 192.0.0.64.3570: UDP, length 1472
IP 192.0.0.64.3570 > 192.0.0.128.57989: UDP, length 4
...
IP 192.0.0.128.57989 > 192.0.0.64.3570: UDP, length 1088
IP 192.0.0.64.3570 > 192.0.0.128.57989: UDP, length 4
{% endhighlight %}

After downloading the new firmware (in my case it took a minute for a
~40MB file ) the NVR will just reboot with it __with the default
configuration__.

## How to do it

Disclaimer: I decline every responsability on this guide, so I won't
be responsible for your damaged device. Also keep in mind that it is
not based on any HikVision official resource.

To do it you need a __computer__ (I used a ubuntu server VM, but it
should be possible to do it on every operative system with the proper
tools) where you have __admin rights__, a connection to the NVR (see the
first point for this) and the possibility to reboot the NVR.

As I wrote before this procedure should work also with HikVison
cameras instead of the NVR, but I haven't tested it.

We are going to do basically three things:

1. configure the network so that the device will connect to our
   computer at boot; 
2. run a script so that the computer will reply correctly to the
   device magic UDP packet;
3. run a TFTP server to serve the firmware file;
4. reboot the device and hope it works.

### Configure the network

You need to have a layer 2 (so only switched) connection to the
device, this means that you have to be in the __same network__. The easier
way to do this is to connect to the same switch the NVR is connected
to or to connect directly to the device with a network cable (crossed
if your network card doesn't have Auto MDI-X).

Then you will have to __configure the address `192.0.0.128/24`__ on the
interface, how to do it depends on the operative system you are
using. For example on linux one way to do it is (assuming that `eth0`
is the interface connected to the device):
{% highlight bash on %}
ip addr add 192.0.0.128/24 dev eth0
{% endhighlight %}

### Run the magic UDP replier

You need some program to reply to the device magic UDP packet, I wrote
it (copy-pasting code from the net) in python but I'm sure it could be
written in any reasonable programming language that supports UDP
sockets. My script can be downloaded from
[github](https://raw.githubusercontent.com/epol/script/master/hikvision/udpreply.py).

The program has to listen on the __9978__ UDP port and reply to any
packet with exactly the __same message__. So it is a sort of UDP
version of _echo_.

If you use my script you will need the __python interpreter__ (that
comes by default in all modern linux distros), from terminal you can
run it with:
{% highlight bash on %}
python udpreply.py
{% endhighlight %}

### Run a TFTP server with the firmware

From the HikVision website __download the firmware__ you intend to
upload on the device. Usually the file you download is a compressed
_zip_ containing the firmware as the `digicap.dav` file. You will need
to __extract this file__.

You need also to __run a TFTP server__ (I used `tftpd-hpa` from ubuntu
repository) that will serve the firmware in his root
directory. Configuring the TFTP server (using the
`/etc/default/tftpd-hpa` file in my example) remember to __permit
access to every IPv4 host__ in the network (so it has to bind to the
`0.0.0.0` address).

__Copy the firmware file__ (`digicap.dav`) in the TFTP server root
directory (`/var/lib/tftpboot` in my example) and run/restart the
process.

### Finally flash the NVR

Once it is all ready you can finally flash the device. To do it
__reboot__ (or power on) the device while connected on the network. It
should connect to your computer, download the firmware, install it and
reboot. In my case it took a minute to do it.

If it doesn't work I think that __sniffing packets on the computer__
(I suggest to use [wireshark](https://www.wireshark.org)) is a good
debug method, in particular here there are some possibilities:

* you don't recevive the ARP for _192.0.0.128_: check again your
  connection to the NVR, maybe try to connect directly with a crossed
  cable;
  
* you receive ARP packet but your computer doesn't answer to them:
  check your address, it should be `192.0.0.128` with `/24` subnet
  mask (aka `255.255.255.0`);
  
* ARP is OK, you receive a UDP packet from the NVR but your computer
  doesn't answer to it: check that the python script is running and
  check the port (in my case it was _9978_, but it may change in
  different models);
  
* UDP communication is OK, but TFTP doesn't respond: check that your
  TFTP server is running and receiving connection from any host.

For any other problem keep in mind that I tested my procedure only on
one model of NVR, so __it may be different on your model__.

## Final thoughts

It has been quite fun to figure out how this recovery method works,
having a HikVision tool simpified it (even if it had a broken TFTP
implementation).

It may be interesting to know why they implemented the magic UDP
packet mechanism, maybe it is a way to prevent users from using their
TFTP server instead of theirs or maybe it is just a way to ensure that
a random TFTP server in the network (with the right IP) blocks the NVR
boot process.

One think I don't like is the fact that I wasn't able to find this
procedure in HikVision documentation (but maybe I looked in the wrong
place), also I managed to download the (bugged) tool following
instruction from a YouTube video comment. So not so easy. Maybe the
right way to unbrick their device is to ask their support center, but
why should I wait for their reply?

