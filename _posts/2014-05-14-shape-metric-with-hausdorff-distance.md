---
layout: post
title:  "Shape metric with Hausdorff distance"
date:   2014-05-14 21:04:32 +0200
categories: mathematic
published: false
tags:
  - geodesics
  - hausorff distance
  - math
  - shape metric
license: CC_BY-SA
--- 

I've recently studied shape spaces and their proprieties when we use
the Hausdorff distance metric.

If \\((M,d)\\) is a metric space I define the set of the shapes in \\(M\\) as \\(C\_s(M) = \\left\\{ A \subseteq M \; :\; A \neq \emptyset \wedge A = \bar A \right\}\\). This set is a metric space using the <a title="Hausorff distance" href="http://en.wikipedia.org/wiki/Hausdorff_distance" target="_blank">Hausdorff distance</a> $latex d_H$.

We also define $latex K(M) =\left\{A\subseteq M\; :\; A \neq \emptyset \wedge A \text{ compact}\right\}$

The main points of my work was:
<ol>
	<li>if $latex (M,d)$ is complete then also $latex (C_s(M),d_H)$ is complete</li>
	<li>if $latex (M,d)$ is compact then also $latex (C_s(M), d_H)$ is compact (Blaschke theorem)</li>
	<li>the number of connected components of a compact is a lower semi-continuous function</li>
	<li>the metric $latex (M,d)$ is <a title="Intrinsic metric" href="http://en.wikipedia.org/wiki/Intrinsic_metric" target="_blank">intrinsic</a> if and only if the metric $latex C_s(M),d_H)$ is intrinsic</li>
	<li>if in $latex M$ the closed balls are compact then, for all $latex A,C \in C_s(M)$ both compact exists a geodesic</li>
</ol>
This proprieties are valid for any metric space $latex M$, the following, instead, are true for $latex M = \mathbb{R}^N$ (I haven't tried to prove them in a generic metric space)
<ol>
	<li>$latex d_H$ is invariant for the actions of the isometries of $latex \mathbb{R}^N$</li>
	<li>The orbits in $latex K(\mathbb{R} ^N$ of that actions are closed</li>
	<li>The distance $latex \bar d _H (\left[ A \right] , \left[ C \right] ) = \inf _{\tilde A \in \left[ A \right] , \tilde C \in \left[ C \right]} d_H(\tilde A, \tilde C)$ is a distance in $latex \frac{K(\mathbb{R}^N)}{G}$</li>
	<li>if $latex (K(\mathbb{R}^N),d_H)$ is intrinsic then $latex (\frac{K(\mathbb{R}^N)}{G}, \bar d_H)$ is intrinsic</li>
</ol>
Then I gave some trivial applications (for example <a title="K-means clustering" href="http://en.wikipedia.org/wiki/K-means_clustering" target="_blank">k-means clustering</a>).

All my work on this topic can be downloaded (with all proofs, but unfortunately in italian) from <a title="appunti1" href="http://uz.sns.it/~enrico/colloquio3/appunti1.pdf" target="_blank">http://uz.sns.it/~enrico/colloquio3/appunti1.pdf</a>

I've really enjoyed studying shape spaces, but unfortunately I haven't
had too much time to study them... maybe another time...
