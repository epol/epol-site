---
layout: post
title:  "Palo Alto firewall user identification with Meru wireless controller"
date:   2016-01-23 11:36:00 +0100
categories: networking
tags: 
  - 802.1x
  - meru
  - palo alto firewall
  - firewall
  - user identification
  - syslog
license: CC_BY-SA
---

When a user connects on a WPA2 Enterprise network the 802.1x mechanism
identifies him, we may want to transfer this information to the
firewall because that would give us two advantages: we would be able
to create ad-hoc rules for the user and the session would be logged
with the username field (a useful feature during debugging). Let's see
how to do it with a Meru (now I should say Fortinet) wireless
controller and a Palo Alto firewall.

According to the documentation, System Director (the Meru wireless
controller operative system) supports, from version 6.1-0-3, a
_"syslog based integration with User ID Agent solution of the Palo
Alto Networks Firewall solution"_. Nice feature, but I didn't find
much documentation online about it, so I decided to write some
instruction on how to do it.

What does "syslog based integration" means? We have to configure the
Meru wireless controller (let's call it MC) to send syslog messages to
the firewall (let's call it FW) with the __User-ID ↔ IP-address__
association, then we have to configure the firewall to receive and
parse that messages correctly.

First of all we __configure the MC to log security information about
users__, this is configured in the security profile. To do it we
navigate (from the web interface of the MC) to _Configuration →
Security → Profile_ and we edit the security profile we are interested
in enabling the _Security Logging_ option. Then we have to __configure
the MC to send the syslogs to the FW__, to do this we can connect to
the CLI of MC, enter configuration mode (`configure terminal` command)
and type `syslog-host <FW IP>` where \<FW IP\> is one IP of
FW. Remember to save the running configuration if all is working.

Now the MC is sending a lot of syslog messages (that FW is discarding,
for now), we are interested in something like this: 
{% highlight syslog %}
<142>ALARM: 1453483456l | system | info | ALR | Station Info Update : MAC-Address : 00:11:22:ff:ae:90, User-Name:	username@example.com, AP-Id: 10, AP-Name: AP-10, BSSID:	00:0c:f2:05:3b:72, ESSID: Rete, IP-Type: discovered, IP-Address: 10.0.0.10, L2-Mode: wpa2, L3-Mode: clear, Vlan-Name: MyVlan, Vlan-Tag: 10, FP-device-type Unknown, FP-os-type: Unknown.
{% endhighlight %}

Let's configure the FW, first of all we have to edit the _Interface
Mgmt profile_ of the interface that is receiving the syslog messages
to __accept that messages__, we navigate (from the web interface) to
_Network → Network Profiles → Interface Mgmt_ and enable _User-ID
Syslog Listener-UDP_ on the proper Mgmt profile.

Now we need a rule to __parse the syslog messages__, so we navigate to
_Device → User Identification → User Mapping_ and we edit the _Palo
Alto Networks User ID Agent Setup_ (using the gear button on top
right), in the "Syslog Filters" tab we add a rule with:

 * Event Regex: `.*Station\sInfo\sUpdate`
 * Username Regex: `User-Name:\s([A-Za-z0-9\.@\-\_]+),`
 * Address Regex: `IP-Address:\s([0-9a-f\.\:]+),`

With the parse rule available we can __configure the receiver__. In
the _User Identification_ page we add a server to the _Server
Monitoring_ tab with:

 * __Type:__ _Syslog sender_
 * __Network address:__ The controller IP used to send sylog messages
 * __Connection type:__ _UDP_
 * __Filter:__ the one we just created

After __committing__ the configuration we should now start seeing
(from the monitor tab) some session with the _User-ID_ field not
empty, if it is not working try to reconnect some device to force the
MC to send the syslog message, a useful CLI command of the FW is `show
user server-monitor state all` that show how many syslog messages it
has received and how many of them was auth success messages.

In my opinion this method has two deficiencies (all on the MC side):

 1. it sends information only on IPv4 addresses, so we are still blind on IPv6 sessions;
 2. it is possible (as far as I know) to configure only one syslog receiver, so all the log messages are sent to the FW (and not to a centralized syslog). A workaround to this problem is to configure the MC to send the syslog to an external syslog server that sends a copy of that messages to the FW.
