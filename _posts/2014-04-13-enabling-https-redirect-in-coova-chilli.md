---
layout: post
title:  "Enabling HTTPS redirect in Coova Chilli"
date:   2014-04-13 10:19:12 +0100
categories:
  - networking
  - coova chilli
tags:
  - chilli
  - coova chilli
  - ssl
  - https
license: CC_BY-SA
--- 

In the default configuration Coova Chilli redirects to the captive
portal only HTTP connections, if it receives a HTTPS connection it
simply drop it (with a "it doesn't work" feeling for the user). You
can tell Chilli to redirect also HTTPS connections, but it may be a
bad idea, so here some pros and cons:

__Pros__

 * it redirects https, so the user page won't stuck during loading

__Cons__

 * You haven't a valid certificate for all sites in the internet, so
   you can't send the redirect with a valid certificate (and the
   client browser require a SSL connection, so you must ship some
   certificate), you must use an other certificate and the user
   browser won't accept it
 * Instead of a stuck page the user will have a "invalid certificate"
   page (very, very scaring)

### Enabling HTTPS redirection

If you still want to enable HTTPS redirection then is simple doing it:
you have to insert this lines in your _/etc/chilli/default_ file

{% highlight config %}
# enable ssl redirect
HS_REDIRSSL=on
HS_SSLKEYFILE=/etc/chilli/sslkey/cert.pem
HS_SSLCERTFILE=/etc/chilli/sslkey/cert.pem
{% endhighlight %}

Where _cert.pem_ is a HTTPS certificate (you can generate it with
_openssl_), security and signing isn't very important here because the
browser won't be happy in any case (he is asking for
_https://www.google.com_, not _https://yoursite_).
