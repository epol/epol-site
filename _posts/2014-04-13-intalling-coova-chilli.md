---
layout: post
title:  "Installing Coova Chilli"
date:   2014-04-13 10:05:12 +0100
categories:
  - networking
  - coova chilli
tags:
  - chilli
  - coova chilli
  - install
  - NAT
license: CC_BY-SA
--- 

Coova Chilli is a open-source captive portal software evolved from the
ChilliSpot project (that is no longer maintained). For more
information see the Coova Chilli [page](http://coova.org/CoovaChilli).

I've chosen Coova Chilli for the following features:

 * It's a captive portal (yeah, pretty straightforward);
 * It has a walled garden feature;
 * It has a command line interface (unfortunately not too well documented);
 * Someone recommended me ChilliSpot, since this project is now defunct I thought it was natural to go on Coova Chilli.

I wanted to install it in a Debian machine, in the download section of
Coova Chilli web site they provide a ubuntu _deb_ package, it doesn't
work very well (and it is for i386 architecture). So I decided to
compile it from source. This is how I manage to do it.

##Installation
###Download and unpack it
Straightforward: they got a link to the _tar.gz_ file in their homepage, so

{% highlight bash %}
wget http://ap.coova.org/chilli/coova-chilli-1.3.0.tar.gz
tar -xf coova-chilli-1.3.0.tar.gz
cd coova-chilli-1.3.0
{% endhighlight %}

###Configure it
Now the configuration, after a lot of build (and run) attempt I've figured out that the default prefix ("_/usr/local_") doesn't work very well, so I change it in "" (the _/_ directory, but using the prefix "_/_" would generate a lot of "_//patch/blabla_" configuration patches), I also enabled some features:

 * _largelimits_ because I'm using a "big" computer (3GB RAM) and not a small wifi router with few memory
 * _nfcoova_ for the kernel module
 * _openssl_ to manage ssl connections in redirect
 * _binstatusfile_ it should make the restart faster because it already knows all about the network
 * _statusfile, dnslog, ipv6_ I haven't looked into this features yet

So my _configure_ command is:
{% highlight bash %}
./configure --prefix= --enable-largelimits --enable-binstatusfile \\
	--enable-statusfile --with-nfcoova --with-openssl \\
	--enable-dnslog --with-ipv6
{% endhighlight %}

Before running it I had to install some dependencies: `libssl-dev
iptables-dev linux-headers-amd64` (and maybe something else)
. Unfortunately _configure_ won't always fail if you lack of one of
them, for example I had to trace back a failed _#include_ during
_make_ to figure out that i needed the _iptables-dev_ package.

###Building
After configuring you can compile and install Coova Chilli
{% highlight bash %}
make
make install
{% endhighlight %}


Note: if you need to change some configuration and rebuild Coova I
recommend to to this
{% highlight bash %}
make uninstall
make clean
./configure $MYPARAMS
make
make install
{% endhighlight %}

###Configuring

You need to edit the _/etc/chilli/default_ file configuring, at least,
the network parameters (ip range, interface).

Usually you want your chilli network to communicate to other networks
(for example internet), to do it you have two possibilities:

 * Configure your network (the one with internet and all your
   services) with a route to the chilli network (you can do it with a
   static route in the router or with a dynamic routing protocol like
   ospf)
 * Configure a nat, all your chilli traffic will be see by the rest of the network as traffic of your coovachilli machine, to do this you have to create the file _/etc/chilli/ipup.sh_ with the following line:
{% highlight bash %}
iptables -I POSTROUTING -t nat -o $HS_WANIF -j MASQUERADE
{% endhighlight %}

##Starting and stopping it
By default _CoovaChilli_ needs a _chilli_ user in the system, it's easy to create it in Debian using `useradd`

Starting chilli is simple:
{% highlight bash %}
service chilli start
{% endhighlight %}

To stop it is a bit harder because the standard init script can't find
the pid file (I haven't worked on it yet), so the simpler way to stop
chilli is 
{% highlight bash %}
killall chilli
{% endhighlight %}
